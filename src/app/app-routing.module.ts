import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {LoginComponent} from "./features/login/login.component";
import {HomeComponent} from "./features/home/home.component";
import {AdminComponent} from "./features/admin/admin.component";
import {UsersDetailsComponent} from "./features/users/users-details/users-details.component";
import {AuthGuard} from "./core/auth/auth.guard";
import {PlanGuard} from "./core/auth/plan.guard";

@NgModule({
  imports: [
    RouterModule.forRoot([
      {
        path: 'login',
        loadChildren: () => import('./features/login/login.module').then(m => m.LoginModule)
      },
      { path: 'home', component: HomeComponent  },
      {
        path: 'tvmaze',
        loadChildren: () => import('./features/tvmaze/tvmaze.module').then(m => m.TvmazeModule),
      },
      {
        path: 'admin',
        component: AdminComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'settings',
        loadChildren: () => import('./features/settings/settings.module').then(m => m.SettingsModule),
      },
      {
        path: 'users',
        loadChildren: () => import('./features/users/users.module').then(m => m.UsersModule),
        canActivate: [AuthGuard]
        //pathMatch: 'full'
      },


      { path: '', component: HomeComponent, pathMatch: 'full' },
      { path: 'uikit', loadChildren: () => import('./features/uikit/uikit.module').then(m => m.UikitModule) },
      { path: 'contacts', loadChildren: () => import('./features/contacts/contacts.module').then(m => m.ContactsModule) },
      { path: '**', redirectTo: 'login' }
    ])
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {}
