export interface Auth {
  accessToken: string;
  role: string;
  displayName: string;
}
