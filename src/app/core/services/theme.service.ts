import {Injectable} from "@angular/core";
import {Theme} from "../../model/theme";


@Injectable({
  providedIn: 'root'
})
export class ThemeService {
  private _theme: Theme = 'dark'

  set theme(value: Theme) {
    this._theme = value;
  }

  get theme(): Theme {
    return this._theme
  }

}
