import {Directive, ElementRef, HostBinding, Input, TemplateRef, ViewContainerRef} from "@angular/core";
import {AuthenticationService} from "./authentication.service";
import {filter} from "rxjs/operators";

@Directive({
  selector: '[ifRoleIs]'
})
export class IfRoleIsDirective {
  @Input() ifRoleIs!: string;

  constructor(
    private authService: AuthenticationService,
    private view: ViewContainerRef,
    private tpl: TemplateRef<any>
  ) {}

  ngOnInit(): void {
    this.authService.data
      .subscribe(auth => {
        console.log(auth, this.ifRoleIs)
        if (auth?.role === this.ifRoleIs) {
          this.view.createEmbeddedView(this.tpl)
        } else {
          this.view.clear();
        }
      })
  }
}
