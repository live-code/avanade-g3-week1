import {Injectable} from "@angular/core";
import {CanActivate, Router} from "@angular/router";
import {AuthenticationService} from "./authentication.service";

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(
    private authSrv: AuthenticationService,
    private router: Router
  ) {
  }
  canActivate(): boolean {
    if (!this.authSrv.isLogged) {
      this.router.navigateByUrl('login')
    }
    return this.authSrv.isLogged;
  }
}
