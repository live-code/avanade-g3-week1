import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {Auth} from "../../model/auth";
import {Router} from "@angular/router";
import {BehaviorSubject, Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  auth: Auth | null = null;
  error: boolean = false;
  data = new BehaviorSubject<Auth | null>(null);
  // o----o--------o---->


  constructor(private http: HttpClient, private router: Router) {
    const auth: string | null = localStorage.getItem('auth')
    if (auth) {
      this.auth = JSON.parse(auth)
      this.data.next(JSON.parse(auth))
    }
  }

  login(username: string, password: string) {
    this.error = false;
    const params = new HttpParams()
      .set('user', username)
      .set('pass', password);

    this.http.get<Auth>(`http://localhost:3000/login`, { params } )
      .subscribe(
        res => {
          this.auth = res;
          this.data.next(res)
          localStorage.setItem('auth', JSON.stringify(res))
          this.router.navigateByUrl('admin')
        },
        err => this.error = true
      )
  }

  logout() {
    this.auth = null;
    this.data.next(null)
    this.router.navigateByUrl('login');
    localStorage.removeItem('auth')
  }

  get displayName(): string | undefined {
    return this.auth?.displayName
  }

  get isLogged(): boolean {
    return !!this.auth
  }
}
