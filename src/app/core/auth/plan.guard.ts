import {Injectable} from "@angular/core";
import {CanActivate, Router} from "@angular/router";
import {AuthenticationService} from "./authentication.service";

@Injectable({
  providedIn: 'root'
})
export class PlanGuard implements CanActivate {

  constructor(
    private authSrv: AuthenticationService,
    private router: Router
  ) {
  }

  canActivate(): boolean {
    if (true) {
      console.log('plan guard redirect')
      this.router.navigateByUrl('contacts')
    }
    return false;
  }
}
