import {Directive, ElementRef, HostBinding, TemplateRef, ViewContainerRef} from "@angular/core";
import {AuthenticationService} from "./authentication.service";
import {filter} from "rxjs/operators";

@Directive({
  selector: '[ifLogged]'
})
export class IfLoggedDirective {

  constructor(
    private authService: AuthenticationService,
    private view: ViewContainerRef,
    private tpl: TemplateRef<any>
  ) {
    this.authService.data
      .subscribe(value => {
        if (!!value) {
          view.createEmbeddedView(tpl)
        } else {
          view.clear();
        }
      })
  }
}
