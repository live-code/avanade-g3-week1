import {Injectable} from "@angular/core";
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {Observable, of} from "rxjs";
import {AuthenticationService} from "./authentication.service";
import {Router} from "@angular/router";
import {catchError} from "rxjs/operators";

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(
    private authService: AuthenticationService,
    private router: Router
  ) {
  }
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let clonedReq = req;
    if (this.authService.auth?.accessToken) {
      clonedReq = req.clone({
        setHeaders: {
          'Authorization': 'Bearer ' + this.authService.auth.accessToken
        }
      })
    }

    return next.handle(clonedReq)
      .pipe(
        catchError(err => {
          switch (err.status) {
            case 0:
            case 401:
            case 404:
              this.authService.logout();
              break
          }
          return of(err)
        })
      )
  }

}
