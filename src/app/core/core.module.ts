import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {NavbarComponent} from "./navbar.component";
import {RouterModule} from "@angular/router";
import {IfLoggedDirective} from "./auth/if-logged.directive";
import {IfRoleIsDirective} from "./auth/if-role-is.directive";



@NgModule({
  declarations: [
    NavbarComponent,
    IfLoggedDirective,
    IfRoleIsDirective
  ],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [
    NavbarComponent,
    IfLoggedDirective
  ]
})
export class CoreModule { }
