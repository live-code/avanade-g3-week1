import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {ThemeService} from "./services/theme.service";
import {Theme} from "../model/theme";
import {AuthenticationService} from "./auth/authentication.service";

@Component({
  selector: 'app-navbar',
  // changeDetection: ChangeDetectionStrategy.OnPush,

  template: `


    <nav
      class="navbar navbar-expand"
      [ngClass]="{
        'navbar-light bg-light': theme === 'light',
        'navbar-dark bg-dark': theme === 'dark'
      }"
    >
      <div class="container-fluid">
        <a class="navbar-brand" href="#">Navbar {{theme}}</a>
        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link" *ifLogged [routerLinkActive]="'active'" aria-current="page" routerLink="home">Home</a>
            </li>
            <li class="nav-item" *ifRoleIs="'admin'">
              <a class="nav-link" [routerLinkActive]="'active'"  routerLink="users">Users</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" [routerLinkActive]="'active'"  routerLink="login">Login</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" [routerLinkActive]="'active'"  routerLink="settings">Settings</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" routerLinkActive="active"  routerLink="tvmaze">tvmaze</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" routerLinkActive="active"  routerLink="contacts">contacts</a>
            </li>
            <li class="nav-item"  *ifLogged>
              <a class="nav-link" routerLinkActive="active"  routerLink="admin">admin</a>
            </li>

            <li class="pull-right" (click)="authSrv.logout()">
              <a class="nav-link">{{authSrv.displayName}}</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  `,
})
export class NavbarComponent  {
  @Input() theme!: Theme;

  constructor(public authSrv: AuthenticationService) {
  }
}
