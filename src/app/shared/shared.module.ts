import {NgModule} from "@angular/core";
import {HelloComponent} from "./components/hello.component";
import {CardComponent} from "./components/card.component";
import {ModalComponent} from "./components/modal.component";
import {TabbarComponent} from "./components/tabbar.component";
import {StaticMapComponent} from "./components/static-map.component";
import {BrowserModule} from "@angular/platform-browser";
import {FormsModule} from "@angular/forms";
import {RouterModule} from "@angular/router";
import {CommonModule} from "@angular/common";

@NgModule({
  declarations: [
    HelloComponent,
    CardComponent,
    ModalComponent,
    TabbarComponent,
    StaticMapComponent,
  ],
  exports: [
    HelloComponent,
    CardComponent,
    ModalComponent,
    TabbarComponent,
    StaticMapComponent,
  ],
  imports: [
    CommonModule
  ],
})
export class SharedModule {

}
