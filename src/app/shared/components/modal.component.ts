import {Component, Input, EventEmitter, Output} from '@angular/core';

@Component({
  selector: 'app-modal',
  template: `
    <div class="my-modal">

      <h1>{{title}}</h1>

      <div class="body">
        <ng-content></ng-content>
      </div>

      <i class="closeIcon fa fa-times" (click)="close.emit()"></i>
    </div>
  `,
  styles: [`
    .closeIcon {
      position: fixed;
      right: 20px;
      top: 20px;
      font-size: 2rem;
    }
    .my-modal {
      position: fixed;
      top: 0; right: 0; left: 0; bottom: 0;
      background-color: rgba(255, 255, 255,0.9);
      z-index: 100
    }

    .body {
      width: 600px;
      height: 95vh;
      margin: 0 auto;
      background-color: black;
      color: white;
      overflow: auto;
    }
  `]
})
export class ModalComponent {
  @Input() title: string = '';
  @Input() isOpen: boolean = false;
  @Output() close = new EventEmitter()

}
