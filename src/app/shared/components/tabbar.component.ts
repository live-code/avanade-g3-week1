import {Component, Input, EventEmitter, Output, OnInit} from '@angular/core';

@Component({
  selector: 'app-tabbar',
  template: `
    <ul class="nav nav-tabs">
      <li
        class="nav-item"
        *ngFor="let item of items"
        (click)="tabClick.emit(item)"
      >
        <a
          class="nav-link"
          [ngClass]="{'active': item.id === selectedItem?.id}"
        >{{item[labelField]}} </a>
      </li>
    </ul>
  `,
})
export class TabbarComponent {
  @Input() items: any[] | null = [];
  @Input() selectedItem: any;
  @Input() labelField = 'name';
  @Output() tabClick = new EventEmitter<any>()
}
