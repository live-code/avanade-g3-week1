import {Component, Input, EventEmitter, Output} from '@angular/core';

@Component({
  selector: 'app-card',
  template: `
    <div class="card">
      <!--header-->
      <div
        *ngIf="title"
        class="card-header"
        [ngClass]="headerCls"
        (click)="isOpen = !isOpen"
      >
        {{title}}

        <div class="pull-right" *ngIf="icon" >
          <i [ngClass]="icon" (click)="iconClick.emit()"></i>
        </div>
      </div>

      <!--body-->
      <div class="card-body" *ngIf="isOpen">
        <ng-content></ng-content>
      </div>

    </div>
  `,
  styles: [
  ]
})
export class CardComponent {
  @Input() title: string = '';
  @Input() headerCls!: string;
  @Input() icon!: string;
  @Output() iconClick = new EventEmitter();
  isOpen = true;
}
