import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-hello',
  template: `
    <p>
      hello {{name}}!
    </p>
  `,
})
export class HelloComponent {
  @Input() name: string | number = '';

}
