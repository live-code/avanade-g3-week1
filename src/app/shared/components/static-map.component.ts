import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-static-map',
  template: `
    <img [src]="'https://maps.googleapis.com/maps/api/staticmap?center=' + value + '&zoom=9&size=900x500&key=AIzaSyBzQurvW6YciltmMcXNZwKo5njyFIn_f8I'" alt="">

  `,
  styles: [
  ]
})
export class StaticMapComponent {
  @Input() value!: string | undefined;
}
