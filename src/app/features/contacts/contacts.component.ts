import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-contacts',
  template: `
    <button routerLink="/contacts/form">form</button>
    <button routerLink="/contacts/locations">location</button>

    <hr>

    <router-outlet></router-outlet>

  `,
  styles: [
  ]
})
export class ContactsComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
