import { Component, OnInit } from '@angular/core';

interface Country {
  id: number;
  name: string;
  cities: City[]
}

interface City {
  id: number,
  label: string;
};

@Component({
  selector: 'app-admin',
  template: `

    <app-tabbar
      [items]="countries"
      [selectedItem]="activeCountry"
      (tabClick)="setCountryHandler($event)"
    ></app-tabbar>

    <app-tabbar
      *ngIf="activeCountry?.cities"
      [items]="activeCountry?.cities || []"
      [selectedItem]="activeCity"
      labelField="label"
      (tabClick)="setCityHandler($event)"
    ></app-tabbar>

    <app-static-map [value]="activeCity?.label" *ngIf="activeCity"></app-static-map>
  `,
})
export class AdminComponent  {
  countries: Country[] | null = null;
  activeCountry: Country | null = null;
  activeCity: City | null = null;

  constructor() {
    setTimeout(() => {
      this.countries =  [
        {
          id: 11,
          name: 'Italy',
          cities: [
            { id: 100, label: 'Palermo'},
            { id: 200, label: 'Milan'},
          ]
        },
        {
          id: 22,
          name: 'Spain',
          cities: [
            { id: 1010, label: 'Madrid'},
          ]
        },
        {
          id: 33,
          name: 'Japan',
          cities: [
            { id: 12300, label: 'Tokyo'},
            { id: 2030, label: 'Osaka'},
            { id: 20330, label: 'Kyoto'},
          ]
        }
      ];
      this.setCountryHandler(this.countries[0])
    }, 1000)
  }

  setCountryHandler(item: Country) {
    this.activeCountry = item;
    this.activeCity = this.activeCountry.cities[0];
  }

  setCityHandler(city: City) {
    this.activeCity = city;
  }

}
