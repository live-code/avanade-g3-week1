import { Component, OnInit } from '@angular/core';
import {ThemeService} from "../../core/services/theme.service";
import {Theme} from "../../model/theme";

@Component({
  selector: 'app-settings',
  template: `
    <h1 [ngClass]="{
        'bg-dark text-white': themeService.theme === 'dark',
        'bg-light': themeService.theme === 'light'
    }">Settings</h1>

    <h3>{{themeService.theme}}</h3>

    <button
      (click)="changeThemeHandler('dark')"
      [ngClass]="{'bg-warning': themeService.theme === 'dark'}"
    >dark</button>

    <button
      (click)="changeThemeHandler('light')"
      [ngClass]="{'bg-warning': themeService.theme === 'light'}"
    >light</button>


  `
})
export class SettingsComponent {
  constructor(public themeService: ThemeService) {
  }

  changeThemeHandler(value: Theme) {
    this.themeService.theme = value;

  }

}
