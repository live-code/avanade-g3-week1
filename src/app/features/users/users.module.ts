import {NgModule} from "@angular/core";
import {UsersListComponent} from "./anagrafica/components/users-list.component";
import {UsersFormComponent} from "./anagrafica/components/users-form.component";
import {UsersListItemComponent} from "./anagrafica/components/users-list-item.component";
import {AnagraficaComponent} from "./anagrafica/anagrafica.component";
import {SharedModule} from "../../shared/shared.module";
import {RouterModule} from "@angular/router";
import {FormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";
import { UsersComponent } from './users.component';
import {RolesComponent} from "./roles/roles.component";
import {UsersDetailsComponent} from "./users-details/users-details.component";

@NgModule({
  declarations: [
    UsersComponent,
    RolesComponent,
    UsersDetailsComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild([
      {
        path: '',
        component: UsersComponent,
        children: [
          {
            path: 'anagrafica',
            loadChildren: () => import('./anagrafica/anagrafica.module').then(m => m.AnagraficaModule)
          },
          {
            path: 'roles',
            component: RolesComponent
          },
          { path: ':id', component: UsersDetailsComponent },
          {
            path: '',
            redirectTo: 'roles'
          }
        ]
      },

    ])
  ]
})
export class UsersModule {}
