import {NgModule} from "@angular/core";
import {AnagraficaComponent} from "./anagrafica.component";
import {UsersListComponent} from "./components/users-list.component";
import {UsersListItemComponent} from "./components/users-list-item.component";
import {UsersFormComponent} from "./components/users-form.component";
import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";
import {SharedModule} from "../../../shared/shared.module";
import {RouterModule} from "@angular/router";

@NgModule({
  declarations: [
    AnagraficaComponent,
    UsersListComponent,
    UsersListItemComponent,
    UsersFormComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: AnagraficaComponent
      }
    ])
  ]
})
export class AnagraficaModule {}
