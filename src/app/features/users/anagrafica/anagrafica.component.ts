import { Component } from '@angular/core';
import {UsersService} from "./services/users.service";

@Component({
  selector: 'app-anagrafica',
  template: `

    <div class="mt-2 mx-2">
      <app-card [title]="usersService.selectedUser?.id ? 'EDIT USER:' + usersService.selectedUser?.name : 'ADD NEW USER'">
        <app-users-form
          [selectedUser]="usersService.selectedUser"
          (clear)="usersService.clearHandler()"
          (save)="usersService.save($event)"
        ></app-users-form>
      </app-card>
      <hr>
      <!--USERS LIST-->
      <app-card [title]="usersService.users?.length + ' users'">
        <app-users-list
          [users]="usersService.users"
          (deleteUser)="usersService.deleteUserHandler($event)"
          (setSelectedUser)="usersService.setSelectedUserHandler($event)"
        ></app-users-list>
      </app-card>
    </div>

  `,
})
export class AnagraficaComponent {
  constructor(public usersService: UsersService) {
    this.usersService.getAll()
  }
}
