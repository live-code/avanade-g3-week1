import {Injectable} from "@angular/core";
import {User} from "../model/user";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {NgForm} from "@angular/forms";

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  users: Partial<User>[] = [];
  selectedUser: Partial<User> | null = null;

  constructor(private http: HttpClient) { }

  getAll(): void {
   this.http.get<Partial<User>[]>('http://localhost:3000/users')
     .subscribe(
       (result) => {
         this.users = result;
       },
       (err) => {
         console.log('error')
       }
     )
  }


  deleteUserHandler(user: Partial<User>) {
    this.http.delete(`http://localhost:3000/users/${user.id}`)
      .subscribe(() => {
        if (this.users) {
          this.users = this.users.filter(u => u.id !== user.id)
        }
        this.selectedUser = null;
      })

  }



  save(form: NgForm) {
    if (this.selectedUser?.id) {
      this.edit(form.value)
    } else {
      this.add(form.value)
    }
  }

  add(user: Partial<User>) {
    this.http.post<Partial<User>>(`http://localhost:3000/users`, user)
      .subscribe(res => {
        // this.users.push(res)
        this.users = [...this.users, res]
      })

  }

  edit(user: Partial<User>) {
    this.http.patch<Partial<User>>(`http://localhost:3000/users/${this.selectedUser?.id}`, user)
      .subscribe(res => {
        const index = this.users.findIndex(u => u.id === this.selectedUser?.id)
        this.users[index] = res;
      })

  }


  setSelectedUserHandler(user: Partial<User>) {
    this.selectedUser = user;
  }

  clearHandler() {
    this.selectedUser = null;
  }

}
