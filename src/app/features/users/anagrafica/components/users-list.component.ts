import {ChangeDetectionStrategy, Component, EventEmitter, Input, Output} from '@angular/core';
import {User} from "../model/user";

@Component({
  selector: 'app-users-list',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `

    <ul class="list-group">

      <app-users-list-item
        *ngFor="let user of users; let i = index; let even = even; let first = first"
        [user]="user"
        [even]="even"
        (deleteUser)="deleteUser.emit($event)"
        (setSelectedUser)="setSelectedUser.emit($event)"
      ></app-users-list-item>

    </ul>

  `,
})
export class UsersListComponent {
  @Input() users!: Partial<User>[];
  @Output() deleteUser = new EventEmitter<Partial<User>>()
  @Output() setSelectedUser = new EventEmitter<Partial<User>>()

}

