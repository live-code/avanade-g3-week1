import {Component, Input, EventEmitter, Output} from '@angular/core';
import {User} from "../model/user";
import {NgForm} from "@angular/forms";

@Component({
  selector: 'app-users-form',
  template: `
    <form #f="ngForm" (submit)="save.emit(f)">
      <!--USER FORM-->
      <div *ngIf="inputName.errors?.required">Campo obbligatorio</div>
      <div *ngIf="inputName.errors?.minlength">Campo corto</div>

      <input
        type="text"
        placeholder="name"
        name="name"
        [ngModel]="selectedUser?.name"
        required
        minlength="3"
        #inputName="ngModel"
        class="form-control"
        [ngClass]="{'is-invalid': inputName.invalid && f.dirty, 'is-valid': inputName.valid}"
      >
      <button type="submit" [disabled]="f.invalid">
        {{selectedUser?.id ? 'EDIT' : 'ADD'}}
      </button>
      <button *ngIf="selectedUser?.id" type="button"
              (click)="clear.emit()">Clear</button>
    </form>
  `,
})
export class UsersFormComponent {
  @Input() selectedUser: Partial<User> | null = null;
  @Output() clear = new EventEmitter()
  @Output() save = new EventEmitter<NgForm>()
}
