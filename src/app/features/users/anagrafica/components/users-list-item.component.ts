import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {User} from "../model/user";

@Component({
  selector: 'app-users-list-item',
  template: `
    <li
      class="list-group-item "
      [ngClass]="{'list-group-item-secondary': even}"
      (click)="selectHandler(user)"
    >
      <i class="fa fa-arrow-circle-down mx-2" (click)="toggleHandler($event)"></i>

      <i class="fa fa-mars" *ngIf="user.gender === 'M'"></i>
      <i class="fa fa-venus" *ngIf="user.gender === 'F'"></i>

      {{user.name}} {{user.id}}
      <span class="badge bg-primary rounded-pill">{{user.phone}}</span>
      <div class="pull-right">
            <span class="badge bg-danger rounded-pill"
                  (click)="deleteHandler(user, $event)">
               <i class="fa fa-trash"></i>
            </span>
        <span class="badge bg-info rounded-pill"
              [routerLink]="'/users/' + user.id">
               <i class="fa fa-arrow-circle-right"></i>
            </span>
      </div>

      <div *ngIf="isOpen">
        <app-static-map [value]="user?.address?.geo?.lat + ',' + user?.address?.geo?.lng"></app-static-map>
      </div>
    </li>
  `,
  styles: [
  ]
})
export class UsersListItemComponent {
  @Input() even!: boolean;
  @Input() user!: Partial<User>;
  @Output() deleteUser = new EventEmitter<Partial<User>>()
  @Output() setSelectedUser = new EventEmitter<Partial<User>>()

  isOpen = false;


  toggleHandler(event: MouseEvent) {
    event.stopPropagation();
    this.isOpen = !this.isOpen;
  }

  deleteHandler(user: Partial<User>, event: MouseEvent) {
    event.stopPropagation();
    this.deleteUser.emit(user)
  }

  selectHandler(user: Partial<User>) {
    this.setSelectedUser.emit(user)
  }


}
