import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { User } from '../anagrafica/model/user';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-users-details',
  template: `

    <ng-template #errorMsg>
      <div class="alert alert-danger">No data!</div>
    </ng-template>

    <ng-container *ngIf="user; else errorMsg">
      <h1>{{user?.name}}</h1>
      <h1>{{user?.phone}}</h1>

      <form #f="ngForm" (submit)="saveHandler(f)">
        <input type="text" [ngModel]="user?.name" name="name" required minlength="3">
        <input type="text" [ngModel]="user?.phone" name="phone" required>
        <button type="submit" [disabled]="f.invalid">SAVE</button>
      </form>

    </ng-container>

    <hr>

    <button routerLink="/users">back</button>

  `,
})
export class UsersDetailsComponent implements OnInit {
  user: Partial<User> | null = null;
  error: boolean = false;

  constructor(private activatedRoute: ActivatedRoute, private http: HttpClient) {
    const id = activatedRoute.snapshot.params.id;
    http.get<Partial<User>>(`http://localhost:3000/users/${id}`)
      .subscribe(
        res => {
          this.user = res;
        },
        err => {
          this.error = true;
        }
      )
  }

  ngOnInit(): void {
  }

  saveHandler(f: NgForm) {
    const id = this.activatedRoute.snapshot.params.id;

    this.http.patch<Partial<User>>(`http://localhost:3000/users/${id}`, f.value)
      .subscribe(res => {
        this.user = res;
      })
  }
}
