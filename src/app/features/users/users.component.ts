import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-users',
  template: `
    <button routerLink="/users/anagrafica">anagrafica</button>
    <button routerLink="/users/roles">roles</button>

    <hr>
    <router-outlet></router-outlet>
  `,
  styles: [
  ]
})
export class UsersComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
