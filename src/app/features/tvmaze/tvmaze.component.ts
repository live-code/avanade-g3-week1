import { Component, OnInit } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {TvSeries} from "./model/tv-series";
import {NgForm} from "@angular/forms";

@Component({
  selector: 'app-tvmaze',
  template: `
    <app-tv-maze-search (search)="searchHandler($event)"></app-tv-maze-search>
    <app-tv-maze-result [series]="items" (itemClick)="openDetails($event)"></app-tv-maze-result>

    <app-modal *ngIf="activeItem" (close)="activeItem = null">
      <app-tv-maze-modal  [activeItem]="activeItem"></app-tv-maze-modal>
    </app-modal>
  `,
})
export class TvmazeComponent  {
  items: TvSeries[] = [];
  activeItem: TvSeries | null = null;

  constructor(private http: HttpClient) {
    this.searchHandler('friends')
  }

  searchHandler(text: string) {
    if (text.length) {
      this.http.get<TvSeries[]>(`https://api.tvmaze.com/search/shows?q=${text}`)
        .subscribe(res => {
          this.items = res;
        })
    }
  }


  openDetails(series: TvSeries) {
    this.activeItem = series;
  }
}
