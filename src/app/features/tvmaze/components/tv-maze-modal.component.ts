import {ChangeDetectionStrategy, Component, Input} from '@angular/core';
import {TvSeries} from "../model/tv-series";

@Component({
  selector: 'app-tv-maze-modal',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <div  *ngIf="activeItem" >
      <img [src]="activeItem.show.image.original" width="100%" alt="">
      <h1>{{activeItem.show.name}}</h1>
      <h1 *ngIf="activeItem.show.rating">Average: {{activeItem.show.rating.average}}</h1>
      <a [href]="activeItem.show.url" target="_blank">Go to website</a>
      <div [innerHTML]="activeItem.show.summary">...</div>
    </div>
  `,
  styles: [
  ]
})
export class TvMazeModalComponent  {
  @Input() activeItem: TvSeries | null = null;
}
