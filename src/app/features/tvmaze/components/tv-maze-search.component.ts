import {Component, OnInit, EventEmitter, Output} from '@angular/core';

@Component({
  selector: 'app-tv-maze-search',
  template: `
    <form #f="ngForm" (submit)="search.emit(f.value.text)">
      <input ngModel name="text" type="text" placeholder="Search TV Series">
    </form>
  `,
  styles: [
  ]
})
export class TvMazeSearchComponent implements OnInit {
  @Output() search = new EventEmitter<string>()
  constructor() { }

  ngOnInit(): void {
  }

}
