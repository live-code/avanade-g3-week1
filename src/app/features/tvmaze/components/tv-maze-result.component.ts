import {Component, Input, OnInit, EventEmitter, Output} from '@angular/core';
import {TvSeries} from "../model/tv-series";

@Component({
  selector: 'app-tv-maze-result',
  template: `
    <div class="wrapper">
      <div class="grid">
        <div *ngFor="let item of series" (click)="itemClick.emit(item)" class="grid-item" >
          <img width="100" *ngIf="item.show.image; else noimage" [src]="item.show.image.medium" alt="">
          <div>{{item.show.name}}</div>
        </div>
      </div>
    </div>

    <ng-template #noimage>
      <div class="no-image"></div>
    </ng-template>
  `,
  styles: [`
    .wrapper {
      width: 100%;
      /*overflow-x: auto;*/
    }

    .grid {
      display: flex;
      flex-wrap: wrap;
      justify-content: center;
    }

    .grid-item {
      text-align: center;
    }

    .no-image {
      width: 100px;
      height: 140px;
      border: 1px solid black;
      background: repeating-linear-gradient(
        -55deg,
        #222,
        #222 10px,
        #333 10px,
        #333 20px
      );
    }
  `]
})
export class TvMazeResultComponent implements OnInit {
  @Input() series: TvSeries[] = [];
  @Output() itemClick = new EventEmitter<TvSeries>()

  constructor() { }

  ngOnInit(): void {
  }

}
