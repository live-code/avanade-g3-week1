import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {TvmazeComponent} from "./tvmaze.component";
import {TvMazeResultComponent} from "./components/tv-maze-result.component";
import {TvMazeSearchComponent} from "./components/tv-maze-search.component";
import {TvMazeModalComponent} from "./components/tv-maze-modal.component";
import {SharedModule} from "../../shared/shared.module";
import {FormsModule} from "@angular/forms";
import {RouterModule} from "@angular/router";



@NgModule({
  declarations: [
    TvmazeComponent,
    TvMazeResultComponent,
    TvMazeSearchComponent,
    TvMazeModalComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    RouterModule.forChild([
      { path: '', component: TvmazeComponent}
    ])
  ]
})
export class TvmazeModule { }
