import { Component } from '@angular/core';

@Component({
  selector: 'app-login',
  template: `


    <h1>Login</h1>

    <router-outlet></router-outlet>

    <div>
      <button routerLink="/login/signin">sign in</button>
      <button routerLink="/login/registration">registration</button>
      <button routerLink="/login/lostpass">forgot pass</button>
    </div>
  `
})
export class LoginComponent {
  section = 'signin'

}
