import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from "../../../core/auth/authentication.service";

@Component({
  selector: 'app-signin',
  template: `
    <h1>Sign In</h1>

    <div *ngIf="authenticationService.error">
      Authentication Failed
    </div>
    <form #f="ngForm" (submit)="authenticationService.login(f.value.username, f.value.password)">
      <input ngModel type="text" name="username" required>
      <input [ngModel] type="password" name="password" required>
      <button [disabled]="f.invalid" type="submit">LOG IN</button>
    </form>
  `,
  styles: [
  ]
})
export class SigninComponent implements OnInit {
  error: boolean = false;

  constructor(public authenticationService: AuthenticationService) { }

  ngOnInit(): void {
  }

}
