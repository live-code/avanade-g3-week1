import {Component, OnDestroy, OnInit} from '@angular/core';
import {HelloComponent} from "../../shared/components/hello.component";
import {HttpClient} from "@angular/common/http";
import {catchError, filter, map} from "rxjs/operators";
import {User} from "../users/anagrafica/model/user";
import {of} from "rxjs";

@Component({
  selector: 'app-home',
  template: `

    <app-card
      title="Profile"
      headerCls="bg-dark text-white"
      icon="fa fa-user"
      (iconClick)="openUrl('http://www.google.com')"
    >
      <app-hello></app-hello>
    </app-card>

    <app-card title="Stats"
              icon="fa fa-bar-chart"
              (iconClick)="open = true"
    >
      <button (click)="open = true">open modal</button>
    </app-card>


    <pre>{{open}}</pre>

    <app-modal
      *ngIf="open"
      title="user profile"
      (close)="open = false"
    >
      my modal
    </app-modal>


    total users: {{users?.length}}
  `,
})
export class HomeComponent implements OnInit, OnDestroy {
  value = 'Fabio'
  open = false;
  users: User[] | null = null;

  constructor(private http: HttpClient) {

    // -----o---xo----o----o--->
   /* this.http.get<User[]>('http://localhost:3000/userss')
      .pipe(
        catchError(err => {
          return of([])
        })
      )
      .subscribe(res => this.users = res)*/
  }

  ngOnInit(): void {
    console.log('init');
  }

  doSomething() {
    alert('clicked')
  }

  openUrl(url: string) {
    window.open(url)
  }

  ngOnDestroy(): void {
    console.log('DESTROY')
  }
}
